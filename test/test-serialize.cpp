#include "JsonValue.h"
#include <sstream>
#include <cassert>
#include <cstring>

static void assert_serialize(const char *expected, const JsonValue &val) {
    std::ostringstream stream;
    stream << val;
    assert(!strcmp(stream.str().c_str(), expected));
}

int main() {
    assert_serialize("5", 5);
    assert_serialize("null", nullptr);
    assert_serialize("[]", JsonArray());
    assert_serialize("{}", JsonObject());

    JsonObject obj;
    obj.set("some_int", 42);
    obj.set("some_null", nullptr);
    JsonArray arr;
    arr.push(35);
    arr.push("This is a string");
    obj.set("some_arr", arr);
    assert_serialize("{\"some_arr\": [35, \"This is a string\"], \"some_int\": 42, \"some_null\": null}", obj);
}
