#include "JsonValue.h"
#include <cassert>

static JsonValue assert_parse(const char *str) {
    JsonValue val = nullptr;
    bool ok = val.parse(str);
    assert(ok);
    assert(!*str);
    return val;
}

int main() {
    JsonValue val = assert_parse("null");
    assert(val.type() == JsonType::Null);

    val = assert_parse("[]");
    assert(val.type() == JsonType::Array);
    assert(val.get_array().size() == 0);

    val = assert_parse("{}");
    assert(val.type() == JsonType::Object);
    assert(val.get_object().size() == 0);

    val = assert_parse(" [ 5   , -10 , \" hello   \" , {  \"a key \"  : true }]");
    assert(val.type() == JsonType::Array);
    const JsonArray &arr = val.get_array();
    assert(arr.size() == 4);
    assert(arr.get(0).type() == JsonType::Int);
    assert(arr.get(0).get_int() == 5);
    assert(arr.get(1).type() == JsonType::Int);
    assert(arr.get(1).get_int() == -10);
    assert(arr.get(2).type() == JsonType::String);
    assert(arr.get(2).get_string() == " hello   ");
    assert(arr.get(3).type() == JsonType::Object);
    const JsonObject &obj = arr.get(3).get_object();
    assert(obj.size() == 1);
    obj.for_each([](const std::string &key, const JsonValue &value) {
        assert(key == "a key ");
        assert(value.type() == JsonType::Bool);
        assert(value.get_bool());
    });
}
