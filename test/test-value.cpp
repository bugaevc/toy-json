#include "JsonValue.h"

int main() {
    JsonValue int_value = 42;
    JsonValue str_value = std::string("a long string that does not fit into inline storage");
    JsonValue str_value_copy = str_value;
    JsonArray arr;
    arr.push(str_value);
    JsonValue arr_value = arr;
    JsonValue arr_value_copy = arr_value;
    JsonObject obj;
    obj.set("some long-ish key string", arr_value_copy);
    obj.set("another interesting key", 42);
    JsonObject obj_copy = obj;
    JsonValue obj_value = obj_copy;
}
