#pragma once

#include <string>

void skip_whitespace(const char *&s);
bool parse_string(const char *&s, std::string &result);
