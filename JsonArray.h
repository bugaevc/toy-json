#pragma once

#include <vector>
#include <iostream>

#include "JsonUtil.h"

class JsonValue;

class JsonArray {
public:
    TOYJSON_API void push(JsonValue);
    size_t size() const { return m_items.size(); }
    const JsonValue &get(size_t index) const { return m_items.at(index); }

    template<typename F>
    void for_each(F f) const {
        for (const JsonValue &item : m_items) {
            f(item);
        }
    }

    TOYJSON_API bool parse(const char *&s);

private:
    std::vector<JsonValue> m_items;
};

TOYJSON_API std::ostream &operator <<(std::ostream &stream, const JsonArray &arr);
