#include "JsonArray.h"
#include "JsonValue.h"
#include "JsonParse.h"
#include <cctype>

void JsonArray::push(JsonValue value) {
    m_items.push_back(std::move(value));
}

std::ostream &operator <<(std::ostream &stream, const JsonArray &arr) {
    stream << "[";
    bool first = true;
    arr.for_each([&](const JsonValue &value) {
        if (!first) {
            stream << ", ";
        }
        stream << value;
        first = false;
    });
    stream << "]";
    return stream;
}

bool JsonArray::parse(const char *&s) {
    skip_whitespace(s);
    if (*s != '[') {
        return false;
    }
    s++;
    while (true) {
        skip_whitespace(s);
        if (*s == ']') {
            s++;
            return true;
        }
        JsonValue item = nullptr;
        bool ok = item.parse(s);
        if (!ok) {
            return false;
        }
        push(std::move(item));
        skip_whitespace(s);
        if (*s == ']') {
            s++;
            return true;
        } else if (*s != ',') {
            return false;
        }
        s++;
    }
}
