#pragma once

#include <string>
#include <cstdint>
#include <cstddef>
#include <iostream>

#include "JsonType.h"
#include "JsonArray.h"
#include "JsonObject.h"
#include "JsonUtil.h"

class JsonValue {
public:
    TOYJSON_API ~JsonValue();
    TOYJSON_API JsonValue(const JsonValue &);
    TOYJSON_API JsonValue(JsonValue &&);
    TOYJSON_API JsonValue &operator =(const JsonValue &);
    TOYJSON_API JsonValue &operator =(JsonValue &&);

    TOYJSON_API JsonValue(std::nullptr_t);
    TOYJSON_API JsonValue(int64_t);
    TOYJSON_API JsonValue(bool);
    TOYJSON_API JsonValue(std::string);
    TOYJSON_API JsonValue(JsonArray);
    TOYJSON_API JsonValue(JsonObject);

    JsonValue(int i) : JsonValue((int64_t) i) { }
    JsonValue(const char *s) : JsonValue(std::string(s)) { }

    JsonType type() const { return m_type; };

    int64_t get_int() const { return m_int; }
    double get_float() const { return m_float; }
    bool get_bool() const { return m_bool;  }
    const std::string &get_string() const { return m_string; }
    const JsonArray &get_array() const { return m_array; }
    const JsonObject &get_object() const { return m_object; }

    TOYJSON_API bool parse(const char *&s);

private:
    JsonType m_type;
    union {
        int64_t m_int;
        double m_float;
        bool m_bool;
        std::string m_string;
        JsonArray m_array;
        JsonObject m_object;
    };
};

TOYJSON_API std::ostream &operator <<(std::ostream &stream, const JsonValue &val);
