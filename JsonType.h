#pragma once

enum class JsonType {
    Null,
    Int,
    Float,
    Bool,
    String,
    Array,
    Object,
};
