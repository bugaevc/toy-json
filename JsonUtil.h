#pragma once

#if __GNUC__ >= 4
#define TOYJSON_API __attribute__((visibility("default")))
#else
#define TOYJSON_API
#endif
