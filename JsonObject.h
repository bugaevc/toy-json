#pragma once

#include <map>
#include <string>
#include <functional>

#include "JsonUtil.h"

class JsonValue;

class JsonObject {
public:
    TOYJSON_API void set(std::string, JsonValue);
    size_t size() const { return m_items.size(); }

    TOYJSON_API void for_each(std::function<void(const std::string &, const JsonValue &)> f) const;

    TOYJSON_API bool parse(const char *&s);

private:
    std::map<std::string, JsonValue> m_items;
};

TOYJSON_API std::ostream &operator <<(std::ostream &stream, const JsonObject &obj);
