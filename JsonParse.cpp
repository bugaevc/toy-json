#include "JsonParse.h"
#include <string>
#include <cctype>

void skip_whitespace(const char *&s) {
    while (*s && isspace((unsigned char) *s)) {
        s++;
    }
}

bool parse_string(const char *&s, std::string &result) {
    skip_whitespace(s);
    if (*s != '"') {
        return false;
    }
    s++;
    const char *start = s;
    // TODO: escaping
    while (*s && *s != '"') {
        s++;
    }
    if (!*s) {
        return false;
    }
    result = std::string(start, s - start);
    s++;
    return true;
}
