#include "JsonObject.h"
#include "JsonValue.h"
#include "JsonParse.h"

void JsonObject::set(std::string key, JsonValue value) {
    m_items.insert({std::move(key), std::move(value)});
}

void JsonObject::for_each(std::function<void(const std::string &, const JsonValue &)> f) const {
    // TODO: investigate why this can't be declared in the header as a template
    for (const auto &pair : m_items) {
        f(pair.first, pair.second);
    }
}

std::ostream &operator <<(std::ostream &stream, const JsonObject &obj) {
    stream << "{";
    bool first = true;
    obj.for_each([&](const std::string &key, const JsonValue &value) {
        if (!first) {
            stream << ", ";
        }
        // TODO: quoting
        stream << '"' << key << "\": " << value;
        first = false;
    });
    stream << "}";
    return stream;
}

bool JsonObject::parse(const char *&s) {
    skip_whitespace(s);
    if (*s != '{') {
        return false;
    }
    s++;
    while (true) {
        skip_whitespace(s);
        if (*s == '}') {
            s++;
            return true;
        }
        std::string key;
        bool ok = parse_string(s, key);
        if (!ok) {
            return false;
        }
        skip_whitespace(s);
        if (*s != ':') {
            return false;
        }
        s++;
        JsonValue value = nullptr;
        ok = value.parse(s);
        if (!ok) {
            return false;
        }
        set(std::move(key), std::move(value));
        skip_whitespace(s);
        if (*s == '}') {
            s++;
            return true;
        } else if (*s != ',') {
            return false;
        }
        s++;
    }
}
