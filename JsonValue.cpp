#include "JsonValue.h"
#include "JsonParse.h"

#include <cassert>
#include <cstring>

JsonValue::JsonValue(std::nullptr_t)
    : m_type(JsonType::Null)
{ }

JsonValue::JsonValue(bool b)
    : m_type(JsonType::Bool)
    , m_bool(b)
{ }

JsonValue::JsonValue(int64_t i)
    : m_type(JsonType::Int)
    , m_int(i)
{ }

JsonValue::JsonValue(std::string s)
    : m_type(JsonType::String)
    , m_string(std::move(s))
{ }

JsonValue::JsonValue(JsonArray arr)
    : m_type(JsonType::Array)
    , m_array(std::move(arr))
{ }

JsonValue::JsonValue(JsonObject obj)
    : m_type(JsonType::Object)
    , m_object(std::move(obj))
{ }

JsonValue::JsonValue(const JsonValue &other)
    : m_type(other.m_type)
{
    switch (m_type) {
    case JsonType::Null:
        break;
    case JsonType::Int:
        m_int = other.m_int;
        break;
    case JsonType::Float:
        m_float = other.m_float;
        break;
    case JsonType::Bool:
        m_bool = other.m_bool;
        break;
    case JsonType::String:
        new(&m_string) std::string(other.m_string);
        break;
    case JsonType::Array:
        new(&m_array) JsonArray(other.m_array);
        break;
    case JsonType::Object:
        new(&m_object) JsonObject(other.m_object);
        break;
    }
}

JsonValue::JsonValue(JsonValue &&other)
    : m_type(other.m_type)
{
    switch (m_type) {
    case JsonType::Null:
        break;
    case JsonType::Int:
        m_int = other.m_int;
        break;
    case JsonType::Float:
        m_float = other.m_float;
        break;
    case JsonType::Bool:
        m_bool = other.m_bool;
        break;
    case JsonType::String:
        new(&m_string) std::string(std::move(other.m_string));
        break;
    case JsonType::Array:
        new(&m_array) JsonArray(std::move(other.m_array));
        break;
    case JsonType::Object:
        new(&m_object) JsonObject(std::move(other.m_object));
        break;
    }
}

JsonValue &JsonValue::operator =(const JsonValue &other)
{
    if (m_type != other.m_type) {
        this->~JsonValue();
        new(this) JsonValue(other);
        return *this;
    }
    switch (m_type) {
    case JsonType::Null:
        break;
    case JsonType::Int:
        m_int = other.m_int;
        break;
    case JsonType::Float:
        m_float = other.m_float;
        break;
    case JsonType::Bool:
        m_bool = other.m_bool;
        break;
    case JsonType::String:
        m_string = other.m_string;
        break;
    case JsonType::Array:
        m_array = other.m_array;
        break;
    case JsonType::Object:
        m_object = other.m_object;
        break;
    }
    return *this;
}

JsonValue &JsonValue::operator =(JsonValue &&other)
{
    if (m_type != other.m_type) {
        this->~JsonValue();
        new(this) JsonValue(std::move(other));
        return *this;
    }
    switch (m_type) {
    case JsonType::Null:
        break;
    case JsonType::Int:
        m_int = other.m_int;
        break;
    case JsonType::Float:
        m_float = other.m_float;
        break;
    case JsonType::Bool:
        m_bool = other.m_bool;
        break;
    case JsonType::String:
        m_string = std::move(other.m_string);
        break;
    case JsonType::Array:
        m_array = std::move(other.m_array);
        break;
    case JsonType::Object:
        m_object = std::move(other.m_object);
        break;
    }
    return *this;
}

JsonValue::~JsonValue() {
    switch (m_type) {
    case JsonType::Null:
    case JsonType::Int:
    case JsonType::Float:
    case JsonType::Bool:
        break;
    case JsonType::String:
        m_string.~basic_string();
        break;
    case JsonType::Array:
        m_array.~JsonArray();
        break;
    case JsonType::Object:
        m_object.~JsonObject();
        break;
    }
}

std::ostream &operator <<(std::ostream &stream, const JsonValue &val) {
    switch (val.type()) {
    case JsonType::Null:
        return stream << "null";
    case JsonType::Int:
        return stream << val.get_int();
    case JsonType::Float:
        return stream << val.get_float();
    case JsonType::Bool:
        return stream << (val.get_bool() ? "true" : "false");
    case JsonType::String:
        // TODO: escaping
        return stream << '"' << val.get_string() << '"';
    case JsonType::Array:
        return stream << val.get_array();
    case JsonType::Object:
        return stream << val.get_object();
    }
    assert(false);
}

bool JsonValue::parse(const char *&s) {
    skip_whitespace(s);
    if (*s == '[') {
        m_type = JsonType::Array;
        new(&m_array) JsonArray;
        return m_array.parse(s);
    } else if (*s == '{') {
        m_type = JsonType::Object;
        new(&m_object) JsonObject;
        return m_object.parse(s);
    } else if (*s == '"') {
        m_type = JsonType::String;
        new(&m_string) std::string;
        return parse_string(s, m_string);
    } else if (!strncmp(s, "null", 4)) {
        m_type = JsonType::Null;
        s += 4;
        return true;
    } else if (!strncmp(s, "true", 4)) {
        m_type = JsonType::Bool;
        m_bool = true;
        s += 4;
        return true;
    } else if (!strncmp(s, "false", 5)) {
        m_type = JsonType::Bool;
        m_bool = false;
        s += 5;
        return true;
    } else if (*s == '-' || isdigit((unsigned char) *s)) {
        char *endptr = nullptr;
        int64_t value = strtoll(s, &endptr, 10);
        // TODO: floats, exponents...
        if (endptr == s) {
            return false;
        }
        m_type = JsonType::Int;
        m_int = value;
        s = endptr;
        return true;
    } else {
        return false;
    }
}
